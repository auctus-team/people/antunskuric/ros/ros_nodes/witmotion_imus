#!/usr/bin/env python
import rospy
import tf
import bluetooth
import pywitmotion as wit

import threading

def imu_to_tf(soc, world_frame, imu_frame,):
    # initial read - to make sure imu is well connected
    data = soc.recv(1024)
    while True:
        data = soc.recv(1024)
        for msg in reversed(data.split(b'U')):
            q = wit.get_quaternion(msg)
            if q is not None:
                br = tf.TransformBroadcaster()
                br.sendTransform((0, 0, 0),
                                q,
                                rospy.Time.now(),
                                imu_frame,
                                world_frame)
                break

       
def witmotion_tf():
    rospy.init_node('witmotion_tf', anonymous=True)

    # word frame get from parameter
    world_frame = rospy.get_param("/world_frame","world")

    # get imus list
    imus = rospy.get_param("/imus",None)
    if imus is None:
        rospy.logerr("List of imus empty - quitting")
        return    

    # connect to all imus
    for name_imu, imu in imus.items():
        rospy.loginfo("Connecting to device : {} - {}".format(name_imu,imu))
        s = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
        try:
            s.connect((imu['address'], 1))
            print("Device {} connected!". format(name_imu))
        except :
            print("Device {} not available!". format(name_imu))
            continue

        t = threading.Thread(target=imu_to_tf, args=(s, world_frame, imu['frame'],))
        t.start()
        
    while not rospy.is_shutdown():
        pass

if __name__ == '__main__':
    try:
        witmotion_tf()
    except rospy.ROSInterruptException:
        pass